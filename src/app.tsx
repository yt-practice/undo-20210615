import { useEffect, useState } from 'preact/hooks'
import { of, Subject } from 'rxjs'
import { concatMap, scan, shareReplay } from 'rxjs/operators'
import { createUndo } from './undo'

interface State {
	page: string
	values: Record<string, { nums: number[]; cur: number }>
}

const init = (): State => ({
	page: 'a',
	values: {
		a: { nums: [0], cur: 1 },
		b: { nums: [0], cur: 1 },
		c: { nums: [0], cur: 1 },
		d: { nums: [0], cur: 1 },
		e: { nums: [0], cur: 1 },
	},
})

const selectpage = (page: string) => muts.next({ is: 'page.select', page })
const add = () => muts.next({ is: 'value.inc' })

type Mut =
	| { is: 'page.select'; page: string }
	| { is: 'value.inc' }
	| { is: 'value.undo' }
	| { is: 'value.redo' }

const unreachable = (v: never) => {
	console.error(v)
	throw new Error(`unreachable.`)
}

const cvalues = (st: State) => {
	const ls = st.values[st.page]
	if (ls) return ls
	throw new Error('unknown.')
}

const gval2 = (v: { nums: number[]; cur: number }) => {
	const n = v.nums[v.cur - 1]
	if (undefined !== n) return n
	throw new Error('unknown.')
}

const gval = (st: State) => gval2(cvalues(st))

const reducer = (st: State, mut: Mut): State => {
	switch (mut.is) {
		case 'page.select':
			return { ...st, page: mut.page }
		case 'value.inc': {
			const cv = cvalues(st)
			const nums = [...cv.nums.slice(0, cv.cur), gval2(cv) + 1]
			const cur = nums.length
			const values = { ...st.values, [st.page]: { nums, cur } }
			return { ...st, values }
		}
		case 'value.undo': {
			const cv = cvalues(st)
			const nums = cv.nums
			const cur = Math.max(1, cv.cur - 1)
			const values = { ...st.values, [st.page]: { nums, cur } }
			return { ...st, values }
		}
		case 'value.redo': {
			const cv = cvalues(st)
			const nums = cv.nums
			const cur = Math.min(nums.length, cv.cur + 1)
			const values = { ...st.values, [st.page]: { nums, cur } }
			return { ...st, values }
		}
		default:
			return unreachable(mut)
	}
}

const muts = new Subject<Mut>()
const globalState = muts.pipe(
	concatMap(a => of(a)),
	scan(reducer, init()),
	shareReplay<State>(1),
)

const pages = [...'abcde']
const h = createUndo()
h.$.subscribe(i => {
	// console.log({ i })
	switch (i) {
		case 'historyUndo':
			muts.next({ is: 'value.undo' })
			break
		case 'historyRedo':
			muts.next({ is: 'value.redo' })
			break
		default:
			break
	}
})

export const App = () => {
	const [state, setstate] = useState<State | null>(null)
	useEffect(() => {
		const s = globalState.subscribe(s => setstate(s))
		selectpage('a')
		return () => s.unsubscribe()
	}, [])
	if (null === state) return null
	return (
		<div>
			{pages.map(page => (
				<button onClick={() => selectpage(page)}>{page}</button>
			))}
			<hr />
			page: {state.page}, value: {gval(state)}
			<br />
			<button onClick={add}>add</button>
			<p>
				<input type="text" />
			</p>
			<table>
				<thead>
					<tr>
						<th>page</th>
						<th>cur</th>
						<th>nums</th>
					</tr>
				</thead>
				<tbody>
					{Object.entries(state.values).map(t => (
						<tr>
							<th>{t[0]}</th>
							<td>{t[1].cur}</td>
							<td>{t[1].nums.join(',')}</td>
						</tr>
					))}
					<tr></tr>
				</tbody>
			</table>
		</div>
	)
}
