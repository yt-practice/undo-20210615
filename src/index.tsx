import { render } from 'preact'
import { App } from './app'

export const main = async () => {
	const main = document.createElement('main')
	document.body.appendChild(main)
	render(<App />, main)
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
})
