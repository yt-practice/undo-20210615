/* eslint-disable @typescript-eslint/no-explicit-any */

export interface Observable<T> {
	/** Subscribes to the sequence with an observer */
	subscribe(observer: Observer<T>): Subscription
	/** Subscribes to the sequence with callbacks */
	subscribe(
		onNext?: (value: T) => void,
		onError?: (error: any) => void,
		onComplete?: () => void,
	): Subscription
}

export interface Subscription {
	/** Cancels the subscription */
	unsubscribe(): void
	/** A boolean value indicating whether the subscription is closed */
	readonly closed: boolean
}

export interface NextObserver<T> {
	next: (value: T) => void
	error?: (err: any) => void
	complete?: () => void
}
export interface ErrorObserver<T> {
	next?: (value: T) => void
	error: (err: any) => void
	complete?: () => void
}
export interface CompletionObserver<T> {
	next?: (value: T) => void
	error?: (err: any) => void
	complete: () => void
}
export type Observer<T> =
	| NextObserver<T>
	| ErrorObserver<T>
	| CompletionObserver<T>
