import { Subject } from 'rxjs'
import { shareReplay } from 'rxjs/operators'

type InputType = 'historyUndo' | 'historyRedo'
export type Undo = ReturnType<typeof createUndo>
export const createUndo = () => {
	const { document } = window
	const ctrl = document.createElement('div')
	ctrl.setAttribute('aria-hidden', 'true')
	ctrl.style.opacity = '0'
	ctrl.style.position = 'fixed'
	ctrl.style.top = '-1000px'
	ctrl.style.pointerEvents = 'none'
	ctrl.tabIndex = -1
	ctrl.contentEditable = 'true'
	ctrl.textContent = '0'
	ctrl.style.visibility = 'hidden'
	document.body.appendChild(ctrl)
	ctrl.addEventListener('focus', () => {
		// Safari では待つ必要があり、すぐに blur できない。
		setTimeout(() => ctrl.blur(), 0)
	})
	let pending = false
	const read = () => Number(ctrl.textContent) || 0
	const sub = new Subject<InputType>()
	const handle = (e: InputEvent) => {
		switch (e.inputType) {
			case 'historyUndo':
				sub.next(e.inputType)
				exec(() => {
					document.execCommand('redo')
				}, false)
				break
			case 'historyRedo':
				sub.next(e.inputType)
				exec(() => {
					document.execCommand('undo')
				}, false)
				break
			default:
				console.log({ type: e.inputType })
				break
		}
	}
	const exec = (main: () => void, notify = false) => {
		const previousFocus = document.activeElement
		if (!notify) pending = true
		try {
			ctrl.style.visibility = ''
			ctrl.focus()
			main()
		} finally {
			pending = false
			ctrl.style.visibility = 'hidden'
			if (previousFocus instanceof HTMLElement) previousFocus.focus()
		}
	}
	exec(() => {
		document.execCommand('selectAll')
		document.execCommand('insertText', false, String(read() + 1))
	}, false)
	exec(() => {
		document.execCommand('selectAll')
		document.execCommand('insertText', false, String(read() + 1))
	}, false)
	exec(() => {
		document.execCommand('undo')
	}, false)
	ctrl.addEventListener('input', e => {
		if (!pending) handle(e as InputEvent)
		// 選択をクリアする。そうしないと、ユーザーのコピージェスチャによって値がコピーされる。
		// 注意。これはおそらく SD では機能しない。
		// 注意。これは、可視性を 'hidden' に設定するということによって軽減される。
		const s = window.getSelection()
		if (s?.containsNode(ctrl, true)) s.removeAllRanges()
	})
	const remove = () => {
		sub.complete()
		document.body.removeChild(ctrl)
	}
	return {
		remove,
		$: shareReplay<InputType>(1)(sub),
	}
}
