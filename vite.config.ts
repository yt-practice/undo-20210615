import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
	esbuild: {
		jsxInject: `import React from 'preact/compat'`,
	},
})
